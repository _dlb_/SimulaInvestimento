# Automação UI/API Simulação Investimento.

Automação usando java11 junit5 e gradle;

Como executar a aplicação:

- Certifique-se de ter o Gradle instalado e adicionado ao PATH de seu sistema operacional, assim como o Git.

- git clone https://gitlab.com/_dlb_/SimulaInvestimento.git

- No terminal, execute a seguinte operação:

- mvn eclipse:eclipse No Eclipse/STS, importe o projeto com o projeto Gradle.

- Com o projeto aberto na IDE, execute em ...SimulaInvestimento\src\test\Feature\SimulaInvestimento.feature
- 

Para desenvolvimento do projeto, foi utilizado o framework Selenium WebDriver com Java,  Junit e Cucumber para escreve o BDD.


Para o teste de aceitação foi desenvolvido o primeiro cenário como Smoke Teste afim de validar que a página foi carregada.


Os demais cenários validam os requisitos do sistema conforme títulos do cenários abaixo:

***Simular investimento por mês (Positivo);***

***Simular investimento com valor inferior a R$ 20,00 (Negativo)***

Simular investimento por ano (Positivo);

Simular investimento para empresa por mês (Positivo);

Simular investimento por ano faltando campo valor investir (Negativo);

Simular investimento por mês faltando campo valor aplicar (Negativo);


Para teste de serviço, foi criado o cenário (Validar retorno da API) que recebe o retorno da API e compara com os dados esperados imprimindo na tela o retorno dos meses e valores recebidos.